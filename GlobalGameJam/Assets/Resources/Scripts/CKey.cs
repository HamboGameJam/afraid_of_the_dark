﻿using UnityEngine;
using System.Collections;

public class CKey : MonoBehaviour {

	Quaternion quaternionRotationCurrent;
	Quaternion quaternionRotationNext;
	float      fTimeStart;
	float      fTimeCurrent;
	float      fTimeLapse = 5.0f;

	void Start ()
	{
		quaternionRotationCurrent = Random.rotation;
		quaternionRotationNext = Random.rotation;
		fTimeStart = Time.time;
	}
	

	void FixedUpdate()
	{
		fTimeCurrent = Time.time;
		Debug.Log("Key:Update ((fTimeCurrent)/(fTimeStart+fTimeLapse))"+((fTimeCurrent)/(fTimeStart+fTimeLapse)));


		if(  ( fTimeCurrent - fTimeStart ) > fTimeLapse  )
		{
			fTimeStart = fTimeStart + fTimeLapse;
			quaternionRotationCurrent = quaternionRotationNext;
			quaternionRotationNext = Random.rotation;
		}
		transform.rotation = Quaternion.Slerp( quaternionRotationCurrent, quaternionRotationNext, ((fTimeCurrent-fTimeStart)/(fTimeLapse)) );

	}

}
