﻿using UnityEngine;
using System.Collections;

public class StateBehaviourGameplayChangeRoom : StateBehaviour
{
	public CSharedData sharedData;

	void Start()
	{
	
	}	

	void Update ()
	{
	
	}

	override public void OnEnabled()
	{
		InstantiateGameObjects();
		SpawnLevel();
		SpawnPlayer();
		SpawnGUI();

		base.StateMachineParent.StateActive = "Main";
	}
	
	override public void OnDisabled()
	{
		
	}

	private void InstantiateGameObjects()
	{
		if( sharedData.goPlayer == null )
		{
			sharedData.goPlayer = (GameObject)Instantiate( Resources.Load("Prefabs/Player") );
			sharedData.goPlayer.name = "Player"; //Defaults to Player(Clone)
		}

		if( sharedData.goRoom == null )
		{
			sharedData.goRoom = (GameObject)Instantiate( Resources.Load("Prefabs/Room_v2") );

			CTeleporter teleporter = (CTeleporter)( sharedData.goRoom.GetComponent("CTeleporter") );

			CTeleporter.sharedData = sharedData;
			CTeleporter.stateMachine = base.StateMachineParent;

		}

	}
	private void SpawnGUI()
	{
		sharedData.goKey.active = sharedData.isKeyFound;
	}

	private void SpawnLevel()
	{
		CRoomObj objRoom = sharedData.goRoom.GetComponent<CRoomObj>();
		//Debug.Log( "objRoom - sharedData.roomDataCurrent.strBackgroundImage " + sharedData.roomDataCurrent.strBackgroundImage );
		//Debug.Break();

		objRoom.wallsTextures = sharedData.roomDataCurrent.strBackgroundImage;
		objRoom.floorTextures = sharedData.roomDataCurrent.strFloorTexture;

		SpawnLevel_Doors( objRoom );
	}

	private void SpawnLevel_Doors( CRoomObj objRoom )
	{
		bool[]  arrbActiveDoors = { false, false, false, false };
		CRoomData[] arrRoomData = { null,  null,  null,  null };

		CRoomData objRoomDataAdjacency;

		objRoomDataAdjacency = sharedData.roomDataCurrent.arrRoomDataAdjacency[0];
		if( objRoomDataAdjacency != null )
		{
			arrbActiveDoors[0] = true;
			arrRoomData[0]     = objRoomDataAdjacency;
		}

		objRoomDataAdjacency = sharedData.roomDataCurrent.arrRoomDataAdjacency[1];
		if( objRoomDataAdjacency != null )
		{
			arrbActiveDoors[1] = true;
			arrRoomData[1]     = objRoomDataAdjacency;
		}

		objRoomDataAdjacency = sharedData.roomDataCurrent.arrRoomDataAdjacency[2];
		if( objRoomDataAdjacency != null )
		{
			arrbActiveDoors[2] = true;
			arrRoomData[2]     = objRoomDataAdjacency;
		}

		objRoomDataAdjacency = sharedData.roomDataCurrent.arrRoomDataAdjacency[3];
		if( objRoomDataAdjacency != null )
		{
			arrbActiveDoors[3] = true;
			arrRoomData[3]     = objRoomDataAdjacency;
		}

		/*
		Debug.Log( "sharedData.roomDataCurrent.arrRoomDataAdjacency[0] == null "+
		          ( sharedData.roomDataCurrent.arrRoomDataAdjacency[0] == null )         );
		Debug.Log( "sharedData.roomDataCurrent.arrRoomDataAdjacency[1] == null "+
		          ( sharedData.roomDataCurrent.arrRoomDataAdjacency[1] == null )         );
		Debug.Log( "sharedData.roomDataCurrent.arrRoomDataAdjacency[2] == null "+
		          ( sharedData.roomDataCurrent.arrRoomDataAdjacency[2] == null )         );
		Debug.Log( "sharedData.roomDataCurrent.arrRoomDataAdjacency[3] == null "+
		          ( sharedData.roomDataCurrent.arrRoomDataAdjacency[3] == null )         );

		Debug.Log( "arrbActiveDoors[0] "+( arrbActiveDoors[0] ) );
		Debug.Log( "arrbActiveDoors[1] "+( arrbActiveDoors[1] ) );
		Debug.Log( "arrbActiveDoors[2] "+( arrbActiveDoors[2] ) );
		Debug.Log( "arrbActiveDoors[3] "+( arrbActiveDoors[3] ) );
		*/



		objRoom.EnableDoors( arrbActiveDoors );
		objRoom.EnableTeleports( arrRoomData );

	}
	private void SpawnPlayer()
	{

		sharedData.goPlayer.transform.position = sharedData.GetSpawnPosition();
		sharedData.goPlayer.rigidbody.velocity = Vector3.zero;
		sharedData.goPlayer.rigidbody.velocity = Vector3.zero;

		Debug.Log( "StateBehaviourGameplayChangeRoom:sharedData.goPlayer.transform.position (" +
		          		sharedData.goPlayer.transform.position.x +", "+
		          		sharedData.goPlayer.transform.position.y +", "+
		          		sharedData.goPlayer.transform.position.z +") "  );

		sharedData.goPlayer.SetActive( true );
		sharedData.goPlayer.GetComponent<CPlayer>().isLit = sharedData.isLit;
	}

	void OnGUI()
	{
		string strGameplay = "StateBehaviourGameplayChangeRoom";
		GUI.Box( new Rect((Screen.width) - 150,0,150,50), strGameplay  );
	}
}
