﻿using UnityEngine;
using System.Collections;

public class CPlayer : MonoBehaviour {

	public GameObject gamoObjectLight;
	private bool bIsLit;

	void Awake()
	{
		/*
		goCubeTeleport = trCubeTeleport.gameObject;

		Transform trSpawnPoint = transform.Find("SphereSpawnPoint") as Transform;
		goSpawnPoint = trSpawnPoint.gameObject;

		Transform trModel = transform.Find("Group100") as Transform;
		goModel = trModel.gameObject;

		Transform trAntiqueDoor = transform.Find("AntiqueDoor") as Transform;
		goAntiqueDoor = trAntiqueDoor.gameObject;
		goAntiqueDoor.active = false;

		this.isVisible = false;
		 */
		Transform trSphereLight = transform.Find("SphereLight") as Transform;
		gamoObjectLight = trSphereLight.gameObject;
		bIsLit = false;
		//gamoObjectLight
	}

	public bool isLit
	{
		get
		{
			return bIsLit;
		}

		set
		{
			bIsLit = value;
			gamoObjectLight.active = value;

		}

	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
