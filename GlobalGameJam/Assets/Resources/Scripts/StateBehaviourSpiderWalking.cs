﻿using UnityEngine;
using System.Collections;

public class StateBehaviourSpiderWalking : StateBehaviour
{	
	//public CStateMachine stateMachineParent;
	public Animation animation;
	private float fDistance;
	private float fSpeed;


	void Awake()
	{

	}

	void Start()
	{
		//
	}
	

	void Update()
	{
		Debug.Log ("StateBehaviourSpiderWalking:Update");
		fDistance -= fSpeed * Time.deltaTime;
		
		if( fDistance >= 0 )
		{
			transform.position += (transform.forward * (fSpeed * Time.deltaTime) );
		}
		else
		{
			RandomState( "turning|idle|walk|walk", StateMachineParent );
		}
	}

	private void RandomState( string strBarSeparatedStateNames, CStateMachine stateMachine )
	{
		string[] arrStrStateNames = strBarSeparatedStateNames.Split('|');
		string strStateName = arrStrStateNames[ Random.Range(0, arrStrStateNames.Length) ];
		
		StateMachineParent.StateActive = strStateName;
	}

	override public void OnEnabled()
	{
		fDistance = Random.Range( 0.5f, 5f );
		fSpeed = Random.Range( -4f, 4f );
		
		animation["walk"].wrapMode = WrapMode.Loop;
		animation.CrossFade("walk");
	}
	
	override public void OnDisabled()
	{
		
	}
}
