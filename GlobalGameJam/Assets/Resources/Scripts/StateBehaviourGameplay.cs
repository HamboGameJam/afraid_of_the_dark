using UnityEngine;
using System.Collections;

public class StateBehaviourGameplay : StateBehaviour
{
	private CSharedData   sharedData;
	private CStateMachine stateMachine;

	void Awake()
	{
		sharedData   = gameObject.AddComponent("CSharedData") as CSharedData;
		stateMachine = gameObject.AddComponent("CStateMachine") as CStateMachine;

		stateMachine.enabled = true;

		State stateGameplayInitialization  = stateMachine.StateInsert( "Initialization" );
		State stateGameplayChangeRoom      = stateMachine.StateInsert( "ChangeRoom" );
		State stateGameplayMain            = stateMachine.StateInsert( "Main" );


		StateBehaviourGameplayInitialization stateBehaviourGameplayInitialization =
			gameObject.AddComponent( "StateBehaviourGameplayInitialization" ) as StateBehaviourGameplayInitialization;
		stateBehaviourGameplayInitialization.sharedData = sharedData;
		( (StateBehaviour)stateBehaviourGameplayInitialization ).StateMachineParent = stateMachine;


		StateBehaviourGameplayEnableInput stateBehaviourGameplayEnableInput =
			gameObject.AddComponent( "StateBehaviourGameplayEnableInput" ) as StateBehaviourGameplayEnableInput;
		( (StateBehaviour)stateBehaviourGameplayEnableInput ).StateMachineParent = stateMachine;


		StateBehaviourGameplayMain stateBehaviourGameplayMain =
			gameObject.AddComponent( "StateBehaviourGameplayMain" ) as StateBehaviourGameplayMain;
		( (StateBehaviour)stateBehaviourGameplayMain ).StateMachineParent = stateMachine;
		stateBehaviourGameplayInitialization.sharedData = sharedData;


		StateBehaviourGameplayChangeRoom stateBehaviourGameplayChangeRoom =
			gameObject.AddComponent( "StateBehaviourGameplayChangeRoom" ) as StateBehaviourGameplayChangeRoom;
		( (StateBehaviour)stateBehaviourGameplayChangeRoom ).StateMachineParent = stateMachine;
		stateBehaviourGameplayChangeRoom.sharedData = sharedData;

		//////////////////////////////////////////////////////////////////////////////////////////

		stateGameplayInitialization.ScriptInsert( stateBehaviourGameplayInitialization );
		stateGameplayMain.ScriptInsert( stateBehaviourGameplayEnableInput );
		stateGameplayChangeRoom.ScriptInsert( stateBehaviourGameplayChangeRoom );


	}

	void Start()
	{
		stateMachine.StateActive = "Initialization";
	}

	override public void OnEnabled()
	{
	}

	override public void OnDisabled()
	{
	}

	void Update()
	{
		//stateMachine.StateActive = "ChangeRoom";
	}

	void OnGUI()
	{
		string strGameplay = "State Gameplay:\n"+( stateMachine.StateActive );
		GUI.Box( new Rect((Screen.width/2) - 75,0,150,50), strGameplay  );
	}
}