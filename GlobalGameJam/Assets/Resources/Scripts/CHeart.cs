﻿using UnityEngine;
using System.Collections;

public class CHeart : MonoBehaviour {

	float fExpandSpeed = 0.1f;
	float fContractSpeed = 0.02f;
	float fExpandedScale = 1.5f;
	float fContractedScale = .85f;
	public bool bIsContracting = false;
	

	void Start ()
	{
	
	}
	


	void GetScaleValue()
	{

	}

	void Update ()
	{
		//Transform.localScale -= Vector3(0.1,0.1,0.1);

		//float fScaleValue = Mathf.Sin (Time.time);

		//Vector3 v3Scale = new Vector3( fScaleValue, fScaleValue, fScaleValue );

		//gameObject.transform.localScale = v3Scale;

		if( bIsContracting )
		{
			Contract();
		}
		else
		{
			Expand();
		}
	}

	private void Contract()
	{
		float fScaleValue = gameObject.transform.localScale.x;
		float fScaleValueNew;
		float fContractionValue = fContractSpeed * Time.deltaTime;

		if(  (fScaleValue - fContractSpeed) < fContractedScale )
		{
			bIsContracting = false;
		}

		fScaleValueNew = Mathf.Clamp( (fScaleValue - fContractSpeed), fContractedScale, fExpandedScale );
		gameObject.transform.localScale = new Vector3(fScaleValueNew, fScaleValueNew, fScaleValueNew);

	}

	private void Expand()
	{
		float fScaleValue = gameObject.transform.localScale.x;
		float fScaleValueNew;
		float fContractionValue = fExpandSpeed * Time.deltaTime;
		
		if(  (fScaleValue + fExpandSpeed) > fExpandedScale )
		{
			bIsContracting = true;
		}
		
		fScaleValueNew = Mathf.Clamp( (fScaleValue + fExpandSpeed), fContractedScale, fExpandedScale );
		gameObject.transform.localScale = new Vector3(fScaleValueNew, fScaleValueNew, fScaleValueNew);
	}
	

}
