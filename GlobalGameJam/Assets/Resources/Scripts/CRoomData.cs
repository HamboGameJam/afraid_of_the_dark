﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CRoomData
{
	public string    strBackgroundImage;
	public string    strFloorTexture;
	public ArrayList arrListMonster;
	//public ArrayList arrListDoors;
	public Vector2   v2Position;
	public CRoomData[] arrRoomDataAdjacency;



	private CRoomData(){}

	public CRoomData( Vector2 v2Pos )
	{
		arrListMonster = new ArrayList();
		//arrListDoors = new ArrayList();
		strBackgroundImage = "Wallpaper" + ( Random.Range(0, 4) );
		strFloorTexture = "Floor" + ( Random.Range(0, 3) );
		v2Position = v2Pos;
		arrRoomDataAdjacency = new CRoomData[]{ null, null, null, null };
		
	}

	public void Connect( ref CRoomData objRoomOther )
	{
		//determine Other Room Index
		
		if( objRoomOther.v2Position.x > this.v2Position.x )
		{
			this.arrRoomDataAdjacency[0] = objRoomOther;
			objRoomOther.arrRoomDataAdjacency[1] = this;
		}
		else if( objRoomOther.v2Position.x < this.v2Position.x )
		{
			this.arrRoomDataAdjacency[1] = objRoomOther;
			objRoomOther.arrRoomDataAdjacency[0] = this;
		}
		else if( objRoomOther.v2Position.y < this.v2Position.y )
		{
			this.arrRoomDataAdjacency[2] = objRoomOther;
			objRoomOther.arrRoomDataAdjacency[3] = this;
		}
		else if( objRoomOther.v2Position.y > this.v2Position.y )
		{
			this.arrRoomDataAdjacency[3] = objRoomOther;
			objRoomOther.arrRoomDataAdjacency[2] = this;
		}
		
	}


	public int GetRandomEmptySpaceIndex()
	{
		ArrayList arrListEmptySpaceIndexes = new ArrayList();
		int iRandomEmptySpaceIndex = -1;

		CRoomData objRoomData;

		objRoomData = arrRoomDataAdjacency[0];
		if( objRoomData == null )
		{
			arrListEmptySpaceIndexes.Add( 0 );
		}

		objRoomData = arrRoomDataAdjacency[1];
		if( objRoomData == null )
		{
			arrListEmptySpaceIndexes.Add( 1 );
		}

		objRoomData = arrRoomDataAdjacency[2];
		if( objRoomData == null )
		{
			arrListEmptySpaceIndexes.Add( 2 );
		}

		objRoomData = arrRoomDataAdjacency[3];
		if( objRoomData == null )
		{
			arrListEmptySpaceIndexes.Add( 3 );
		}
		
		if( arrListEmptySpaceIndexes.Count > -1 )
		{
			iRandomEmptySpaceIndex = (int)arrListEmptySpaceIndexes[ Random.Range( 0, arrListEmptySpaceIndexes.Count ) ];
		}
		
		return iRandomEmptySpaceIndex;
	}


	public static Vector2 GetRandomPositionOffset()
	{
		int iRandomPosition = Random.Range( 0, 4 );
		Vector2 v2RandomPosition = new Vector2( 0.00f, 0.00f );

		if( iRandomPosition == 0 )
		{
			v2RandomPosition.x = 1.00f;
		}
		else if( iRandomPosition == 1 )
		{
			v2RandomPosition.x = -1.00f;
		}
		else if( iRandomPosition == 2 )
		{
			v2RandomPosition.y = -1.00f;
		}
		else if( iRandomPosition == 3 )
		{
			v2RandomPosition.y = 1.00f;
		}
		
		return v2RandomPosition;
		
	}

	public bool HaveEmptyAdjacentSpaces()
	{
		return (  ( arrRoomDataAdjacency[0] == null ) ||
		          ( arrRoomDataAdjacency[1] == null ) ||
		          ( arrRoomDataAdjacency[2] == null ) ||
		          ( arrRoomDataAdjacency[3] == null )    );
	}

}
