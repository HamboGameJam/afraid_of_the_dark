﻿using UnityEngine;
using System.Collections;

public class CDoor : MonoBehaviour
{

	public GameObject goCubeTeleport;
	public GameObject goSpawnPoint;
	public GameObject goModel;
	public GameObject goAntiqueDoor;
	private bool      bIsVisible;

	public bool isVisible
	{
		get{ return bIsVisible;  }

		set{ bIsVisible = value; 
			 goCubeTeleport.active = value;
			 goSpawnPoint.active = value;
			 goModel.active = value;        }
	}

	void Awake()
	{
		Transform trCubeTeleport = transform.Find("CubeTeleport") as Transform;
		goCubeTeleport = trCubeTeleport.gameObject;

		Transform trSpawnPoint = transform.Find("SphereSpawnPoint") as Transform;
		goSpawnPoint = trSpawnPoint.gameObject;

		Transform trModel = transform.Find("Group100") as Transform;
		goModel = trModel.gameObject;

		Transform trAntiqueDoor = transform.Find("AntiqueDoor") as Transform;
		goAntiqueDoor = trAntiqueDoor.gameObject;
		goAntiqueDoor.active = false;

		this.isVisible = false;
	}

	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
}
