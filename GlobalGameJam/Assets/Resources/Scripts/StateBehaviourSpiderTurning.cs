﻿using UnityEngine;
using System.Collections;

public class StateBehaviourSpiderTurning : StateBehaviour
{
	//public CStateMachine stateMachineParent;
	public Animation animation;
	public float fTurningAngle;
	public float fTurningRate;
	public float fTurningPositiveOrNegativeAngle;


	void Start()
	{
		gameObject.animation["iddle"].wrapMode = WrapMode.Loop;	
	}

	void Update()
	{
		//Debug.Log ("StateBehaviourSpiderTurning:Update");
		fTurningAngle -= fTurningRate * Time.deltaTime;

		if( fTurningAngle >= 0 )
		{
			transform.Rotate(  0, (fTurningRate * Time.deltaTime * fTurningPositiveOrNegativeAngle), 0  );
			//Debug.Log("fTurningRate * Time.deltaTime * fTurningPositiveOrNegativeAngle "+(fTurningRate * Time.deltaTime * fTurningPositiveOrNegativeAngle) );
		}
		else
		{
			RandomState( "turning|turning|idle|walk", StateMachineParent );
		}

	}

	private void RandomState( string strBarSeparatedStateNames, CStateMachine stateMachine )
	{
		string[] arrStrStateNames = strBarSeparatedStateNames.Split('|');
		string strStateName = arrStrStateNames[ Random.Range(0, arrStrStateNames.Length) ];

		StateMachineParent.StateActive = strStateName;
	}

	override public void OnEnabled()
	{
		fTurningRate = Random.Range( 0.0f, 120.5f );
		fTurningAngle = Random.Range(00.0f, 40.0f );
		fTurningPositiveOrNegativeAngle = 1.00f;

		if(  (Random.Range(0,2)) > 0  )
		{
			fTurningPositiveOrNegativeAngle = -1.00f;
		}


		gameObject.animation.Play("iddle");
		
	}
	
	override public void OnDisabled()
	{
		
	}

}
