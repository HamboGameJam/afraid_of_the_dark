﻿using UnityEngine;
using System.Collections;

public class CSharedData : MonoBehaviour
{
	public ArrayList  arrListRoomData;
	public CRoomData  roomDataCurrent;
	public CRoomData  roomDataPrevious;
	public GameObject goPlayer;
	public GameObject goRoom;

	private bool bIsKeyFound;
	private bool bIsLit;

	public GameObject goKey;
	public GameObject goHeart;
	public GameObject goMatches;

	public CRoomObj   roomObj;
	public int        iRoomQty = 5;

	public int matchesQuantity
	{
		set
		{
			goMatches.GetComponent<CMatches>().quantity = value;
		}

		get
		{
			return goMatches.GetComponent<CMatches>().quantity;
		}
	}

	public bool isLit
	{
		get
		{
			return bIsLit;
		}

		set
		{
			bIsLit = value;
			goMatches.GetComponent<CMatches>().isLit = value;

			Debug.Log("47 goPlayer == null"+( goPlayer == null));
			//Debug.Log("48 goPlayer.GetComponent<CPlayer>() == null"+( goPlayer.GetComponent<CPlayer>() == null));
			//goPlayer.GetComponent<CPlayer>().isLit = value;
		}
	}

	public bool isKeyFound
	{
		get
		{
			return bIsKeyFound;
		}

		set
		{
			bIsKeyFound = value;
			goKey.active = value;
		}
	}

	void Awake()
	{
		arrListRoomData = new ArrayList();

		roomObj  = null;
		goPlayer = null;
		goRoom   = null;

		
		goKey = GameObject.Find ("GUIKey");
		goHeart = GameObject.Find ("GUIHeart");
		goMatches = GameObject.Find ("GUIMatch");

		Initialize ();
	}
	
	private void DebugLogRooms()
	{
		CRoomData objRoomData;
		CRoomData objRoomDataAdjacent;

		for( int iRoomIndex = 0;
			     iRoomIndex < this.arrListRoomData.Count;
			     iRoomIndex ++                            )
		{
			objRoomData = (CRoomData)arrListRoomData[iRoomIndex];
			
			Debug.Log( "objRoomData.strBackgroundImage " + objRoomData.strBackgroundImage );
			Debug.Log( "objRoomData.strFloorTexture " + objRoomData.strFloorTexture );
			//Debug.Log( "objRoomData.arrListDoors.Count " + objRoomData.arrListDoors.Count );
			Debug.Log( "objRoomData.arrListDoors.v2Position " + objRoomData.v2Position.x + " " + objRoomData.v2Position.y );
			
			for( int iAdjacencyIndex = 0;
				     iAdjacencyIndex < 4;
				     iAdjacencyIndex ++   )
			{
				objRoomDataAdjacent = objRoomData.arrRoomDataAdjacency[iAdjacencyIndex];
				Debug.Log( "objRoomDataAdjacent["+iAdjacencyIndex+"]"+( objRoomDataAdjacent == null ) );
			}
			
		}
		
	}

	private bool DoesRoomExist( Vector2 v2Position )
	{
		bool bDoesDoesRoomExist = false;
		CRoomData objRoomData;
		
		for( int iRoomDataIndex = 0;
		    iRoomDataIndex < arrListRoomData.Count;
		    iRoomDataIndex ++                       )
		{
			objRoomData = (CRoomData)arrListRoomData[iRoomDataIndex];
			
			if(  ( v2Position.x == objRoomData.v2Position.x )&&
			     ( v2Position.y == objRoomData.v2Position.y )   )
			{
				bDoesDoesRoomExist = true;
				break;
			}
			
		}		
		
		//Debug.Log( "DoesRoomExist " + bDoesDoesRoomExist );

		return bDoesDoesRoomExist;
		
	}

	public Vector3 GetSpawnPosition()
	{
		//Hardcode values
		Vector2 v2Delta = new Vector2();
		GameObject goSpawnSphere = null;

		v2Delta.x = 
			( this.roomDataPrevious.v2Position.x - this.roomDataCurrent.v2Position.x );
		
		v2Delta.y = 
			( this.roomDataPrevious.v2Position.y - this.roomDataCurrent.v2Position.y );

		Debug.Log("92 CSharedData:GetSpawnPosition v2Delta "+ v2Delta.x + "," +
		          										      v2Delta.y   );

		Vector3 v3SpawnPosition = new Vector3( 0.00f, -0.05f,0.00f );

		if( v2Delta.x > 0 )
		{ 
			//goSpawnSphere =  GameObject.FindGameObjectWithTag("SpawnE");
			Debug.Log ("99 GetSpawnPosition E");
			v3SpawnPosition.x =  0.70f;
			v3SpawnPosition.z =  3.70f;
		}
		else if( v2Delta.x < 0 )
		{ 
			//goSpawnSphere =  GameObject.FindGameObjectWithTag("SpawnW");
			Debug.Log ("106 GetSpawnPosition W");
			v3SpawnPosition.x =  0.70f;
			v3SpawnPosition.z = -3.70f;
		}
		else if( v2Delta.y > 0 )
		{ 
			//goSpawnSphere =  GameObject.FindGameObjectWithTag("SpawnN");
			Debug.Log ("113 GetSpawnPosition N");
			v3SpawnPosition.x = -3.00f;
			v3SpawnPosition.z =  0.00f;
		}
		else if( v2Delta.y < 0 )
		{ 
			//goSpawnSphere =  GameObject.FindGameObjectWithTag("SpawnS");
			Debug.Log ("120 GetSpawnPosition S");
			v3SpawnPosition.x = 4.50f;
			v3SpawnPosition.z = 0.00f;
		}

		//Debug.Log("109 goSpawnSphere.name " + goSpawnSphere.name + "");
		//Debug.Log("110 goSpawnSphere.transform.position "+ goSpawnSphere.transform.position.x + "," +
		//          goSpawnSphere.transform.position.y   );

		//return goSpawnSphere.transform.position;

		return v3SpawnPosition;
	}

	void Initialize()
	{

		Initialize_GenerateLevel();
		Initialize_GUI();
	}

	private void Initialize_GUI()
	{
		matchesQuantity = 0;
		isLit = false;
	}

	private void Initialize_GenerateLevel()
	{
		roomDataCurrent  = Initialize_GenerateLevel_FirstRoom();
		roomDataPrevious = Initialize_GenerateLevel_SecondRoom();
		
		roomDataCurrent.Connect( ref roomDataPrevious );
		
		arrListRoomData.Add( roomDataCurrent );
		arrListRoomData.Add( roomDataPrevious );
		
		iRoomQty -= 2;
		
		Debug.Log( " arrListRoomData "+ arrListRoomData );
		Debug.Log( " arrListRoomData.Count "+ arrListRoomData.Count );
		Initialize_GenerateLevel_RemainingRooms();
		Debug.Log( " arrListRoomData.Count "+ arrListRoomData.Count );
		//Initialize_GenerateLevel_SetExitDoor();
		
		//DebugLogRooms();
		
	}
	private void Initialize_GenerateLevel_CreateExitRoom()
	{

		/*
CRoomData objRoomData = arrListRoomData[ Random.Range( 0, arrListRoomData.Count ) ] as CRoomData;

			if( objRoomData.HaveEmptyAdjacentSpaces() )
			{
				int iRandomEmptySpaceIndex = objRoomData.GetRandomEmptySpaceIndex();
				
				Vector2 v2NewRoomPosition = new Vector2( objRoomData.v2Position.x,
				                                         objRoomData.v2Position.y  );

				if( iRandomEmptySpaceIndex == 0 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.x + 1.00f );
					
				}
				else if( iRandomEmptySpaceIndex == 1 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.x - 1.00f );
					
				}
				else if( iRandomEmptySpaceIndex == 2 )
				{
					v2NewRoomPosition.y = ( v2NewRoomPosition.y + 1.00f );
					
				}
				else if( iRandomEmptySpaceIndex == 3 )
				{
					v2NewRoomPosition.y = ( v2NewRoomPosition.y - 1.00f );
					
				}

				if( !DoesRoomExist(v2NewRoomPosition) )
				{
					Initialize_GenerateLevel_CreateRoom( v2NewRoomPosition, ref objRoomData );
				}


			}
		 */
	}

	private CRoomData Initialize_GenerateLevel_FirstRoom()
	{
		CRoomData roomData  = new CRoomData( new Vector2( 0.00f, 0.00f ) );
		
		return roomData;
		
	}
	
	private CRoomData Initialize_GenerateLevel_SecondRoom()
	{
		Vector2 v2RandomPosition = new Vector2();
		v2RandomPosition = CRoom.GetRandomPosition();

		//Debug.Log( "61 v2RandomPosition "+v2RandomPosition.x+","+v2RandomPosition.y+" " );

		CRoomData roomData  = new CRoomData( v2RandomPosition );
		Debug.Log( "64 roomData.v2Position "+roomData.v2Position.x+","+roomData.v2Position.y+" " );
		return roomData;
	}
	


	private void Initialize_GenerateLevel_CreateRoom( Vector2 v2NewRoomPosition, ref CRoomData objRoomParent )
	{
		CRoomData objRoomChild = new CRoomData( v2NewRoomPosition );

		objRoomParent.Connect( ref objRoomChild );

		arrListRoomData.Add( objRoomChild );

		iRoomQty -= 1;

	}

	private void Initialize_GenerateLevel_RemainingRooms()
	{
		while( iRoomQty > 0 )
		{
			CRoomData objRoomData = arrListRoomData[ Random.Range( 0, arrListRoomData.Count ) ] as CRoomData;

			if( objRoomData.HaveEmptyAdjacentSpaces() )
			{
				int iRandomEmptySpaceIndex = objRoomData.GetRandomEmptySpaceIndex();
				
				Vector2 v2NewRoomPosition = new Vector2( objRoomData.v2Position.x,
				                                         objRoomData.v2Position.y  );

				if( iRandomEmptySpaceIndex == 0 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.x + 1.00f );
					
				}
				else if( iRandomEmptySpaceIndex == 1 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.x - 1.00f );
					
				}
				else if( iRandomEmptySpaceIndex == 2 )
				{
					v2NewRoomPosition.y = ( v2NewRoomPosition.y + 1.00f );
					
				}
				else if( iRandomEmptySpaceIndex == 3 )
				{
					v2NewRoomPosition.y = ( v2NewRoomPosition.y - 1.00f );
					
				}

				if( !DoesRoomExist(v2NewRoomPosition) )
				{
					Initialize_GenerateLevel_CreateRoom( v2NewRoomPosition, ref objRoomData );
				}


			}
			else
			{
				//door linking
			}

		}
	}

	void Start()
	{
	
	}

	void Update()
	{
	
	}

}
