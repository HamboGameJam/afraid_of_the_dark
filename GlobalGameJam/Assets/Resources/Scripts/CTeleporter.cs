﻿using UnityEngine;
using System.Collections;

// Go from one location, and into another location

public class CTeleporter : MonoBehaviour
{
	static public CSharedData sharedData;
	static public CStateMachine stateMachine;
	public CRoomData objRoomDataDestination;
	
	void Awake()
	{
		objRoomDataDestination = null;
	}

	public void OnTriggerEnter( Collider other )
	{
		GameObject gameObjectTriggerer = other.transform.gameObject;

		//Debug.Log( "CTeleporter:OnTriggerEnter" );
		//Debug.Log( "gameObjectTriggerer.name - "+gameObjectTriggerer.name );

		if( gameObjectTriggerer.name == "Player" )
		{
			Teleport();
		}

	}

	private void Teleport()
	{
		sharedData.roomDataPrevious = sharedData.roomDataCurrent;
		sharedData.roomDataCurrent = objRoomDataDestination;

		Debug.Log( "36 Teleporting from("+(sharedData.roomDataPrevious.v2Position.x)+","+
		          					   (sharedData.roomDataPrevious.v2Position.y)+") " );
		Debug.Log( "38 to ("+
		          					   (sharedData.roomDataCurrent.v2Position.x)+","+
		          					   (sharedData.roomDataCurrent.v2Position.y)+")" );

		stateMachine.StateActive = "ChangeRoom";
	}

}
