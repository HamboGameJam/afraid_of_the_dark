﻿using UnityEngine;
using System.Collections;

public class CSpider : CMonster
{
	protected Animation _animation;
	public string strState;

	void Awake()
	{
		_animation = GetComponent<Animation>();
		if(!_animation)
		{
			Debug.Log("The character you would like to control doesn't have animations. Moving her might look weird.");
			Debug.Break();
		}
		else
		{
			Debug.Log("_animation is not null.");
		}
		Initialize();
	}

	public void Initialize()
	{	
		base.Initialize();
		//stateMachine = gameObject.AddComponent ("CStateMachine") as CStateMachine;
		//stateMachine.enabled = true;

		StateBehaviourSpiderWalking stateBehaviourSpiderWalking = gameObject.AddComponent( "StateBehaviourSpiderWalking" ) as StateBehaviourSpiderWalking;
		stateBehaviourSpiderWalking.animation = _animation;

		StateBehaviourSpiderIdle stateBehaviourSpiderIdle = gameObject.AddComponent( "StateBehaviourSpiderIdle" ) as StateBehaviourSpiderIdle;
		stateBehaviourSpiderIdle.animation = _animation;

		StateBehaviourSpiderTurning stateBehaviourSpiderTurning = gameObject.AddComponent( "StateBehaviourSpiderTurning" ) as StateBehaviourSpiderTurning;
		stateBehaviourSpiderTurning.animation = _animation;

		//Debug.Log( "CSpide : stateMachine == null "+ (stateMachine == null) );
		//Debug.Log( "CSpide : base.stateMachine == null "+ ( base.stateMachine == null) );
		( (StateBehaviour)stateBehaviourSpiderTurning ).StateMachineParent = stateMachine;
		( (StateBehaviour)stateBehaviourSpiderWalking ).StateMachineParent = stateMachine;
		( (StateBehaviour)stateBehaviourSpiderIdle ).StateMachineParent = stateMachine;

		stateMonsterIdle.ScriptInsert(stateBehaviourSpiderIdle);
		stateMonsterWalking.ScriptInsert(stateBehaviourSpiderWalking);
		stateMonsterTurning.ScriptInsert(stateBehaviourSpiderTurning);

		RandomState( "turning", stateMachine );
	}

	private void RandomState( string strBarSeparatedStateNames, CStateMachine stateMachine )
	{
		string[] arrStrStateNames = strBarSeparatedStateNames.Split('|');
		string strStateName = arrStrStateNames[ Random.Range(0, arrStrStateNames.Length) ];
		strState = strStateName;

		stateMachine.StateActive = strStateName;
	}

	override public void OnCollisionWithLight()
	{
	}
	
	override public void OnLeaveCollisionWithLight()
	{
		
	}
}
