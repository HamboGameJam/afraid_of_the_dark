﻿using UnityEngine;
using System.Collections;

public class CRoomObj : MonoBehaviour {

	GameObject[] goVisibleWalls;
	GameObject   goFloor;
	GameObject[] goDoors;

	void Awake()
	{
		int iWallIndex = 0;

		goVisibleWalls = new GameObject[]{ null, null };
		goDoors        = new GameObject[]{ null, null, null, null };

		foreach( GameObject gameObj in GameObject.FindObjectsOfType<GameObject>() )
		{
			if( gameObj.name == "Wall" )
			{
				goVisibleWalls[iWallIndex++] = gameObj;
			}
			else if( gameObj.name == "Floor" )
			{
				goFloor = gameObj;
			}
			else if( gameObj.name == "DoorE" )
			{
				goDoors[0] = gameObj;
			}
			else if( gameObj.name == "DoorN" )
			{
				goDoors[3] = gameObj;
			}
			else if( gameObj.name == "DoorS" )
			{
				goDoors[2] = gameObj;
			}
			else if( gameObj.name == "DoorW" )
			{
				goDoors[1] = gameObj;
			}
		}
		/*
		Debug.Log( "goDoors[0] == null "+(goDoors[0] == null) );
		Debug.Log( "goDoors[1] == null "+(goDoors[1] == null) );
		Debug.Log( "goDoors[2] == null "+(goDoors[2] == null) );
		Debug.Log( "goDoors[3] == null "+(goDoors[3] == null) );
		*/
	}

	public string floorTextures
	{
		set
		{
			goFloor.renderer.material.mainTexture = Resources.Load<Texture2D>( "Textures/"+value+"_diffuse" );
			goFloor.renderer.material.SetTexture( "_BumpMap",
			                                      Resources.Load<Texture2D>("Textures/"+value+"_specular" ) );
		}
	}

	public string wallsTextures
	{
		set
		{
			goVisibleWalls[0].renderer.material.mainTexture = Resources.Load<Texture2D>( "Textures/"+value );
			goVisibleWalls[1].renderer.material.mainTexture = Resources.Load<Texture2D>( "Textures/"+value );
			goVisibleWalls[0].renderer.material.SetTexture(  "_BumpMap",
			                                                 Resources.Load<Texture2D>("Textures/Wallpaper_specular")  );
			goVisibleWalls[1].renderer.material.SetTexture(  "_BumpMap",
			                                                 Resources.Load<Texture2D>("Textures/Wallpaper_specular")  );
		}
	}

	
	public void EnableDoors( bool[] arrIsDoorAvailable )
	{
		EnableDoor(  goDoors[0], arrIsDoorAvailable[0]  );
		EnableDoor(  goDoors[1], arrIsDoorAvailable[1]  );
		EnableDoor(  goDoors[2], arrIsDoorAvailable[2]  );
		EnableDoor(  goDoors[3], arrIsDoorAvailable[3]  );

	}

	private void EnableDoor( GameObject goDoor, bool bIsAbled )
	{

		CDoor objDoor = goDoor.GetComponent<CDoor>();

		objDoor.isVisible = bIsAbled;


		/*
		Transform[] arrGameObjectAllTransform =
			goDoor.GetComponentsInChildren<Transform>();

		foreach( Transform transfromChild
		         in arrGameObjectAllTransform )
		{
			gameObjectChild = transfromChild.gameObject;


		}
		*/
	}


	public void EnableTeleports( CRoomData[] arrRoomDataTeleportDestinations )
	{
	
		CRoomData objRoomData = arrRoomDataTeleportDestinations[0];
		EnableTeleport(  goDoors[0], ref objRoomData  );
		objRoomData = arrRoomDataTeleportDestinations[1];
		EnableTeleport(  goDoors[1], ref objRoomData  );
		objRoomData = arrRoomDataTeleportDestinations[2];
		EnableTeleport(  goDoors[2], ref objRoomData  );
		objRoomData = arrRoomDataTeleportDestinations[3];
		EnableTeleport(  goDoors[3], ref objRoomData  );

	}


	private void EnableTeleport( GameObject goDoor, ref CRoomData roomDataTeleportDestination )
	{
		if( roomDataTeleportDestination != null )
		{
			Debug.Log( "EnableTeleport:GameObject goDoor " + (goDoor == null) );
			Debug.Log( "EnableTeleport:GameObject goDoor.name " + goDoor.name );
			Debug.Log( "EnableTeleport:CRoomData roomDataTeleportDestination " + (roomDataTeleportDestination == null) );

			if(  (goDoor.name == "DoorE") ||
			     (goDoor.name == "DoorN") ||
			     (goDoor.name == "DoorS") ||
			     (goDoor.name == "DoorW")    )
			{
				GameObject gameObjectCubeTeleport =
					GameObject.FindGameObjectWithTag( goDoor.name );
				//Debug.Log( "138 gameObjectCubeTeleport " + (gameObjectCubeTeleport == null) );
				CTeleporter objTeleporter = gameObjectCubeTeleport.GetComponent<CTeleporter>();
				Debug.Log( "roomDataTeleportDestination (" + 
				           (roomDataTeleportDestination.v2Position.x) + "," +
				           (roomDataTeleportDestination.v2Position.y) + ")"      );
				objTeleporter.objRoomDataDestination = roomDataTeleportDestination;
				//Debug.Log( "141 objTeleporter " + (objTeleporter == null) );
			}


			/////////////////////////////

			//CTeleporter objTeleporter = goDoor.GetComponentInChildren<CTeleporter>();
			//Debug.Log( "EnableTeleport:objTeleporter " + (objTeleporter == null) );

			/*
			Transform[] arrGameObjectAllTransforms = goDoor.GetComponentsInChildren<Transform>();
			//goDoor.transform
			Debug.Log( "arrGameObjectAllTransforms "+arrGameObjectAllTransforms.Length );

			foreach( Transform transformChild in arrGameObjectAllTransforms )
			{
				GameObject gameObjectChild = transformChild.gameObject;

				Debug.Log( "EnableTeleport:gameObjectChild.name " + gameObjectChild.name );

				if(  (gameObjectChild.name == "DoorE")||
				     (gameObjectChild.name == "DoorN")||
				     (gameObjectChild.name == "DoorS")||
				     (gameObjectChild.name == "DoorW")   )
				{
					//
					//Transform[] arrGameObjectChildTransforms = gameObjectChild.GetComponentsInChildren<Transform>();

					//foreach( Transform transformChildOfChild in arrGameObjectChildTransforms )
					//{
					//	Debug.Log( "transformChildOfChild.name " + transformChildOfChild.name );
					//}
					//

					GameObject gameObjectCubeTeleport = GameObject.Find( "CubeTeleport" );

					Debug.Log( " 155 gameObjectCubeTeleport " + (gameObjectCubeTeleport == null) );
				}



				if( gameObjectChild.name == "CubeTeleport" )
				{
					CTeleporter objTeleporter = gameObjectChild.GetComponentInChildren<CTeleporter>();
					Debug.Log( "EnableTeleport:objTeleporter " + (objTeleporter == null) );
					objTeleporter.objRoomDataDestination = roomDataTeleportDestination;
				}

			}
			*/
			//Debug.Break();
			/*
			CTeleporter objTeleporter = goDoor.GetComponentInChildren<CTeleporter>();

			Debug.Log( "EnableTeleport:objTeleporter " + (objTeleporter == null) );
			Debug.Log( "EnableTeleport:objTeleporter.objRoomDataDestination " + (objTeleporter.objRoomDataDestination == null) );

			objTeleporter.objRoomDataDestination = roomDataTeleportDestination;
			*/
		}

	} 


	void Start()
	{
	
	}


	// Update is called once per frame
	void Update()
	{
	
	}

}
