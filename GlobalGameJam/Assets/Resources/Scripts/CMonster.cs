﻿using UnityEngine;
using System.Collections;

public abstract class CMonster : MonoBehaviour
{
	public bool IsIlluminated;
	protected CStateMachine stateMachine;
	protected State stateMonsterIdle;
	protected State stateMonsterWalking;
	protected State stateMonsterTurning;


	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool IsCollidingWithLight( Vector3 v3LightPosition, float fLightRadius )
	{
		return true;
	}

	public abstract void OnCollisionWithLight();
	public abstract void OnLeaveCollisionWithLight();

	virtual public void Initialize()
	{
		stateMachine = gameObject.AddComponent ("CStateMachine") as CStateMachine;
		stateMachine.enabled = true;
		
		stateMonsterIdle =
			stateMachine.StateInsert( "idle" );

		stateMonsterWalking = 
			stateMachine.StateInsert( "walk" );

		stateMonsterTurning = 
			stateMachine.StateInsert( "turning" );


		//RandomState( "turning|idle|walk", stateMachine );
		//StateBehaviourSpiderWalking stateBehaviourSpiderWalking = gameObject.AddComponent( "StateBehaviourGameplayInitialization" ) as StateBehaviourGameplayInitialization;
		//stateBehaviourGameplayInitialization.abacus = abacus;
		//( (StateBehaviour)stateBehaviourGameplayInitialization ).StateMachineParent = stateMachine;
		
	}

	private void RandomState( string strBarSeparatedStateNames, CStateMachine stateMachine )
	{
		string[] arrStrStateNames = strBarSeparatedStateNames.Split('|');
		string strStateName = arrStrStateNames[ Random.Range(0, arrStrStateNames.Length) ];
		
		stateMachine.StateActive = strStateName;
	}
}
