﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CLevel : MonoBehaviour
{
	public GameObject goPlayer;

	public int iRoomQty = 5;
	public CRoom roomfirst;
	public ArrayList arrListRooms;

	void Awake()
	{
		arrListRooms = new ArrayList();
	}

	void Start()
	{
		Generate(roomfirst);
	}

	void Update()
	{
	
	}

	public void Generate_( int iRemainingNumberOfRooms )
	{
		while( iRemainingNumberOfRooms > 0 )
		{
			CRoom objRoom = arrListRooms[ Random.Range( 0, arrListRooms.Count ) ] as CRoom;

			if( objRoom.HaveEmptySpaces() )
			{
				int iRandomSpaceIndex = objRoom.GetRandomEmptySpaceIndex();

				Vector2 v2NewRoomPosition = new Vector2( objRoom.v2Position.x,
				                                         objRoom.v2Position.y  );

				if( iRandomSpaceIndex == 0 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.x + 1.00f );

				}
				else if( iRandomSpaceIndex == 1 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.x - 1.00f );
					
				}
				else if( iRandomSpaceIndex == 2 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.y + 1.00f );
					
				}
				else if( iRandomSpaceIndex == 3 )
				{
					v2NewRoomPosition.x = ( v2NewRoomPosition.y - 1.00f );
					
				}

				Generate_CreateRoom( v2NewRoomPosition, objRoom );
			}
			else
			{
				//objRoom link one existing to another
			}
				/*
		{
			this.Rooms[0] = objRoomOther;
			objRoomOther.Rooms[1] = this;
		}
		else if( objRoomOther.v2Position.x < this.v2Position.x )
		{
			this.Rooms[1] = objRoomOther;
			objRoomOther.Rooms[0] = this;
		}
		else if( objRoomOther.v2Position.y < this.v2Position.y )
		{
			this.Rooms[2] = objRoomOther;
			objRoomOther.Rooms[3] = this;
		}
		else if( objRoomOther.v2Position.y > this.v2Position.y )
		{
			this.Rooms[3] = objRoomOther;
			objRoomOther.Rooms[2] = this;
		}
				 */

		}
	}
	public CRoom GetRoomByPosition( Vector2 v2Position )
	{
		CRoom room = null;

		for( int iRoomIndex = 0;
		         iRoomIndex < arrListRooms.Count;
		         iRoomIndex ++                    )
		{
			room = (CRoom)arrListRooms[ iRoomIndex ];

			if( ( room.v2Position.x == v2Position.x ) &&
			    ( room.v2Position.y == v2Position.y )    )
			{
				break;
			}

		}

		return room;

	}

	public void Generate( CRoom objRoomFirst )
	{
		CRoom objRoomSecond = Generate_CreateRoomSecond();

		Debug.Log( "CLevel:Generate - objRoomFirst == null  " + ( objRoomFirst  == null ) );
		Debug.Log( "CLevel:Generate - objRoomSecond == null " + ( objRoomSecond == null ) );
		//Debug.Break();

		objRoomFirst.Connect(objRoomSecond);

		objRoomFirst.goPlayer  = this.goPlayer;
		objRoomSecond.goPlayer = this.goPlayer;

		arrListRooms.Add( objRoomFirst );
		arrListRooms.Add( objRoomSecond );
		
		iRoomQty -= 2;

		Generate_( iRoomQty );
		/**/
	}

	public void Generate()
	{
		CRoom objRoomFirst = Generate_CreateFirstRoom();
		CRoom objRoomSecond = Generate_CreateRoomSecond();
		objRoomFirst.Connect(objRoomSecond);

		arrListRooms.Add( objRoomFirst );
		arrListRooms.Add( objRoomSecond );

		iRoomQty -= 2;

		Generate_( iRoomQty );
	}
	CRoom Generate_LocateAdjacentRoom( CRoom roomObj )
	{
		bool bAdjacentRoomLocated = true;

		Vector2 v2AdjacentCoordinates = new Vector2( roomObj.v2Position.x,
		                                             roomObj.v2Position.y );

		CRoom roomOther = null;

		while( !bAdjacentRoomLocated )
		{
			int iRandomAdjacentIndex = Random.Range( 0, 4 );

			if( iRandomAdjacentIndex == 0 )
			{
				v2AdjacentCoordinates.x = v2AdjacentCoordinates.x + 1.00f;
			}
			else if( iRandomAdjacentIndex == 1 )
			{
				v2AdjacentCoordinates.x = v2AdjacentCoordinates.x - 1.00f;
			}
			else if( iRandomAdjacentIndex == 2 )
			{
				v2AdjacentCoordinates.x = v2AdjacentCoordinates.y + 1.00f;
			}
			else if( iRandomAdjacentIndex == 3 )
			{
				v2AdjacentCoordinates.x = v2AdjacentCoordinates.y - 1.00f;
			}

			roomOther = GetRoomByPosition( v2AdjacentCoordinates );

			if( roomOther != null )
			{
				bAdjacentRoomLocated = true;
			}
		}

		return roomOther;
	}

	CRoom Generate_CreateRoom( Vector2 v2Position, CRoom roomParent )
	{
		GameObject goRoom = (GameObject)Instantiate( Resources.Load("Prefabs/Room") ); 

		CRoom objRoom = goRoom.GetComponent( "CRoom" ) as CRoom;

		//CRoom objRoom = gameObject.AddComponent( "CRoom" ) as CRoom;

		objRoom.v2Position = new Vector2( v2Position.x, v2Position.y );
		objRoom.enabled = false;
		//Debug.Break();
		/*
		roomParent.Connect( objRoom );
		*/
		iRoomQty -= 1;

		return objRoom;
	}

	CRoom Generate_CreateFirstRoom()
	{
		CRoom objRoom = gameObject.AddComponent( "CRoom" ) as CRoom;
		objRoom.enabled = false;
		objRoom.v2Position = new Vector2( 0.00f, 0.00f );
		
		return objRoom;
	}

	CRoom Generate_CreateRoomSecond()
	{
		GameObject goRoom = Instantiate( Resources.Load("Prefabs/Room") ) as GameObject;
		//goRoom.e
		CRoom objRoom = goRoom.GetComponent( "CRoom" ) as CRoom;
		objRoom.enabled = false;

		//Debug.Break();

		Vector2 v2RandomPosition = CRoom.GetRandomPosition();

		objRoom.v2Position = new Vector2( v2RandomPosition.x,
		                                  v2RandomPosition.y );
		
		return objRoom;
	}

}
