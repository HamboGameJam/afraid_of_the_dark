﻿using UnityEngine;
using System.Collections;

public class CRoom : MonoBehaviour
{
	public GameObject goPlayer;

	//GameObject.FindWithTag("Respawn"); 
	public Vector2 v2Position;
	public GameObject[] goDoors;
	public CDoor[]    objDoors;
	public string   strWallPaperColor;
	public bool[]   Doors;
	public CRoom[] Rooms;
	public int      iSpawnPoint;


	public string strTextures;

	void Awake()
	{
		goPlayer = GameObject.FindWithTag("Player");
		goDoors = GameObject.FindGameObjectsWithTag("Door");


		Rooms = new CRoom[4];
		/**/
		Rooms[0] = null;
		Rooms[1] = null;
		Rooms[2] = null;
		Rooms[3] = null;

		Debug.Log ( "CRoom : goDoors[].length" + goDoors.Length );
		//Debug.Break ();
		//foreach( GameObject goDoor in goDoors )
		//{
			
		//}

		GameObject.FindGameObjectsWithTag("Door");
	}

	void Start()
	{
		OnEnter ();
	}

	void Initialize()
	{

	}

	void OnEnter()
	{
		OnEnter_PlaceCharcaterInSpawnPoint();
	}

	private void OnEnter_PlaceCharcaterInSpawnPoint()
	{
		//Vector3 v3SpawnPoint = GetSpawnPoint();

		goPlayer.transform.position = GetSpawnPoint();
	}
	
	void Update()
	{
	
	}


	public static Vector2 GetRandomPosition()
	{
		int iRandomPosition = Random.Range( 0, 4 );
		Vector2 v2RandomPosition = new Vector2( 0.00f, 0.00f );


		if( iRandomPosition == 0 )
		{
			v2RandomPosition.x = 1.00f;
		}
		else if( iRandomPosition == 1 )
		{
			v2RandomPosition.x = -1.00f;
		}
		else if( iRandomPosition == 2 )
		{
			v2RandomPosition.y = -1.00f;
		}
		else if( iRandomPosition == 3 )
		{
			v2RandomPosition.y = 1.00f;
		}

		return v2RandomPosition;

	}


	public int GetRandomEmptySpaceIndex()
	{
		ArrayList arrListEmptySpaceIndexes = new ArrayList();
		int iRandomEmptySpaceIndex = -1;

		if( Rooms[0] == null )
		{
			arrListEmptySpaceIndexes.Add( 0 );
		}

		if( Rooms[1] == null )
		{
			arrListEmptySpaceIndexes.Add( 1 );
		}
		
		if( Rooms[2] == null )
		{
			arrListEmptySpaceIndexes.Add( 2 );
		}

		if( Rooms[3] == null )
		{
			arrListEmptySpaceIndexes.Add( 3 );
		}

		if( arrListEmptySpaceIndexes.Count > -1 )
		{
			iRandomEmptySpaceIndex = (int)arrListEmptySpaceIndexes[ Random.Range( 0, arrListEmptySpaceIndexes.Count ) ];
		}

		return iRandomEmptySpaceIndex;
	}

	public bool HaveEmptySpaces()
	{
		return (  ( Rooms[0] == null ) ||
		          ( Rooms[1] == null ) ||
		          ( Rooms[2] == null ) ||
		          ( Rooms[3] == null )    );
	}

	private Vector3 GetSpawnPoint()
	{	
		CDoor objDoor = goDoors[ iSpawnPoint ].GetComponent<CDoor>();

		//objDoor.goSpawnPoint.transform.position = ;
		return new Vector3( (objDoor.goSpawnPoint.transform.position.x),
		                    (objDoor.goSpawnPoint.transform.position.y),
		                    (objDoor.goSpawnPoint.transform.position.z) );
	 
	}

	public void Connect( CRoom objRoomOther )
	{
		//determine Other Room Index
		Vector2 v2Delta = new Vector2( (this.v2Position.x + objRoomOther.v2Position.x),
		                               (this.v2Position.y + objRoomOther.v2Position.y)  );

		if( objRoomOther.v2Position.x > this.v2Position.x )
		{
			this.Rooms[0] = objRoomOther;
			objRoomOther.Rooms[1] = this;
		}
		else if( objRoomOther.v2Position.x < this.v2Position.x )
		{
			this.Rooms[1] = objRoomOther;
			objRoomOther.Rooms[0] = this;
		}
		else if( objRoomOther.v2Position.y < this.v2Position.y )
		{
			this.Rooms[2] = objRoomOther;
			objRoomOther.Rooms[3] = this;
		}
		else if( objRoomOther.v2Position.y > this.v2Position.y )
		{
			this.Rooms[3] = objRoomOther;
			objRoomOther.Rooms[2] = this;
		}

	}
	//private Vector3 GetSpawnPoint()
	//{
	/*
		if( iSpawnPoint == 0 )
		{

		}
		else if( iSpawnPoint == 1 )
		{

		}
		else if( iSpawnPoint == 2 )
		{

		}
		else
		{

		}
	 */
	//}
}
