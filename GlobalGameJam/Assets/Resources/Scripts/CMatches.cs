﻿using UnityEngine;
using System.Collections;

public class CMatches : MonoBehaviour
{
	public GameObject gameObjectParticleSystemFire;
	public GameObject gameObjectTextQuantity;
	private int iQuantity;
	private bool bIsLit;

	void Awake()
	{
		iQuantity = 0;

		Transform trParticleSystemFire = transform.Find("ParticleSystemFire") as Transform;
		gameObjectParticleSystemFire = trParticleSystemFire.gameObject;

		Transform trTextQuantity = transform.Find("TextQuantity") as Transform;
		gameObjectTextQuantity = trTextQuantity.gameObject;
	}
	public bool isLit
	{
		get
		{
			return bIsLit;
		}

		set
		{
			bIsLit = value;
			gameObjectParticleSystemFire.active = value;
		}
	}

	public int quantity
	{
		get
		{
			return iQuantity;
		}
		set
		{
			iQuantity = value;
			gameObjectTextQuantity.GetComponent<TextMesh>().text = "X"+( value );
		}
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
