﻿using UnityEngine;
using System.Collections;

public class StateBehaviourSpiderIdle : StateBehaviour
{
	public CStateMachine stateMachineParent;
	public Animation animation;
	private float fTotalTime;

	void Start()
	{
		//animation["idle"].wrapMode = WrapMode.Loop;
		Debug.Log( "StateBehaviourSpiderIdle:Start() gameObject == null"+(gameObject == null)  );
		animation["iddle"].wrapMode = WrapMode.Loop;
	}
	
	void Update()
	{
		Debug.Log ("StateBehaviourSpiderIdle:Update");
		fTotalTime -=Time.deltaTime;
		
		if( fTotalTime <= 0 )
		{
			RandomState( "turning|turning|idle|walk", StateMachineParent );
		}


	}
	
	private void RandomState( string strBarSeparatedStateNames, CStateMachine stateMachine )
	{
		string[] arrStrStateNames = strBarSeparatedStateNames.Split('|');
		string strStateName = arrStrStateNames[ Random.Range(0, arrStrStateNames.Length) ];

		Debug.Log( "StateBehaviourSpiderIdle : StateBehaviour - strStateName " + strStateName );
		Debug.Log( "StateBehaviourSpiderIdle : stateMachineParent == null " + (stateMachineParent == null) );
		StateMachineParent.StateActive = strStateName;
	}

	override public void OnEnabled()
	{
		fTotalTime = Random.Range( 0.25f, .75f );
		gameObject.animation.Play("iddle");
		
	}
	
	override public void OnDisabled()
	{
		
	}
	
}
