using UnityEngine;
using System.Collections;

public class State : MonoBehaviour
{
	private string m_strID;
	
	public string StateID
    {
        get
        {
            return m_strID;
        }

        set
        {
        	m_strID = string.Copy( value );
        }
        
    }

	
    public void OnEnable()
    {
    	if( m_lstScripts.Count > 0 )
        {   
        	foreach(StateBehaviour mbScript in m_lstScripts)
        	{
        		mbScript.enabled = true;
        		mbScript.OnEnabled();
        	}
        }
        
    }
    
    public void OnDisable()
    {
        if( m_lstScripts.Count > 0 )
        {
	        foreach(StateBehaviour mbScript in m_lstScripts)
	        {
	        	mbScript.OnDisabled();
	        	mbScript.enabled = false;
	        }
        }
        
    }
 
 	public void ScriptRemove(StateBehaviour mbScriptInstance)
 	{
 		if( mbScriptInstance != null )
 		{
			StateBehaviour mbScriptCompare;

	        int iCurrentScriptIndex;

	        for( iCurrentScriptIndex = 0;
	             iCurrentScriptIndex < m_lstScripts.Count;
 	             iCurrentScriptIndex ++                    )
 	        { 
            	mbScriptCompare = (StateBehaviour)m_lstScripts[ iCurrentScriptIndex ];
                
            	if( mbScriptInstance == mbScriptCompare )
            	{ 
                	ScriptRemove( iCurrentScriptIndex );
                	break;
            	}
        	}
 		}
 		
 	}

    private void ScriptRemove(int iScriptIndex)
    {
        StateBehaviour objScript = (StateBehaviour)m_lstScripts[ iScriptIndex ];
        StateBehaviour.Destroy( objScript );
        m_lstScripts.RemoveAt( iScriptIndex );
        
    }

	public MonoBehaviour ScriptInsert( StateBehaviour mbScriptInsert )
	{
		StateBehaviour mbScriptReturn = null;
		
        foreach( StateBehaviour mbehaviour in m_lstScripts )
        {
        	if( mbScriptInsert.GetInstanceID() == mbehaviour.GetInstanceID() )
        	{
        		return mbScriptInsert;
        	}
        }
        
        mbScriptReturn = mbScriptInsert;
        m_lstScripts.Add( mbScriptInsert );
        mbScriptInsert.enabled = false;
        
        return mbScriptReturn;
        
    }
	
    public System.Collections.ArrayList m_lstScripts = new System.Collections.ArrayList();	
}
